package com.ahmadi.mokhtar.arturtask.di.modules

import android.content.Context
import androidx.room.Room
import com.ahmadi.mokhtar.arturtask.App
import com.ahmadi.mokhtar.arturtask.data.database.AppDatabase
import com.ahmadi.mokhtar.arturtask.data.database.PostDao
import com.ahmadi.mokhtar.arturtask.data.database.PostRoomDataSource
import com.ahmadi.mokhtar.arturtask.data.network.Api
import com.ahmadi.mokhtar.arturtask.data.network.PostNetworkDataSource
import com.ahmadi.mokhtar.arturtask.data.repository.PostsRepository
import com.ahmadi.mokhtar.arturtask.utils.DATABASE.DATABASE_POST
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    fun providesApp():App = app

    @Provides
    @Singleton
    internal fun providesContext(): Context = app.applicationContext

    @Singleton
    @Provides
    internal fun providesAppDatabase(context: Context) : AppDatabase =
        Room.databaseBuilder(context , AppDatabase::class.java ,DATABASE_POST).build()

    @Provides
    @Singleton
    internal fun providesPostsDaoHelper(appDatabase: AppDatabase): PostDao = appDatabase.postDao()

    @Provides
    @Singleton
    internal fun providesPostRoomDataSource(postDao: PostDao) = PostRoomDataSource(postDao)

    @Provides
    @Singleton
    internal fun providesPostNetworkDataSource(api: Api) = PostNetworkDataSource(api)

    @Provides
    @Singleton
    internal fun providesPostRepository(postNetworkDataSource: PostNetworkDataSource , postRoomDataSource: PostRoomDataSource) =
            PostsRepository(postRoomDataSource, postNetworkDataSource)



}