package com.ahmadi.mokhtar.arturtask.utils

object DATABASE {
    const val DATABASE_POST = "posts.db"
    const val DATABASE_POST_VERSION = 1
    const val PAGE_SIZE = 20
}