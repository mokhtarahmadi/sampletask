package com.ahmadi.mokhtar.arturtask.data.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ahmadi.mokhtar.arturtask.data.models.Post

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieList: List<Post>)

    @Query("SELECT * FROM Post")
    fun allPosts(): DataSource.Factory<Int, Post>
}