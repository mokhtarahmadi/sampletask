package com.ahmadi.mokhtar.arturtask.data.network

import javax.inject.Inject


class PostNetworkDataSource @Inject internal constructor(private val api: Api) {
    fun fetchPosts(page: Int) = api.getPosts(page)
}