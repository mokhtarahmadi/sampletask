package com.ahmadi.mokhtar.arturtask.data.database

import com.ahmadi.mokhtar.arturtask.data.models.Post
import javax.inject.Inject

class PostRoomDataSource @Inject internal constructor(private val postDao: PostDao) {

    fun storePosts(postList: List<Post>) = postDao.insert(postList)

    fun getPosts() = postDao.allPosts()
}