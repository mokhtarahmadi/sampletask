package com.ahmadi.mokhtar.arturtask.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "post")
data class Post(
        @PrimaryKey
        val id:Long,
        val user_name:String?,
        val user_id:String?,
        val user_pic:String?,
        val message:String?,
        val photo:String?,
        val date:String?
):Parcelable