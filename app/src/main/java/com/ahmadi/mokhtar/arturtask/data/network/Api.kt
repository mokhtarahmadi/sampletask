package com.ahmadi.mokhtar.arturtask.data.network

import com.ahmadi.mokhtar.arturtask.data.models.Citymani
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("main/test")
    fun getPosts(@Query("page") page:Int):Single<Citymani>
}