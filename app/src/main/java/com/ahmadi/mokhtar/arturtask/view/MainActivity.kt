package com.ahmadi.mokhtar.arturtask.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ahmadi.mokhtar.arturtask.App
import com.ahmadi.mokhtar.arturtask.R
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.ahmadi.mokhtar.arturtask.view.adapter.PostListAdapter
import com.ahmadi.mokhtar.arturtask.viewModel.PostsViewModel
import com.ahmadi.mokhtar.arturtask.viewModel.PostsViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: PostsViewModel
    private val postListAdapter by lazy { PostListAdapter() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecycler()
        initViewModel()
        initObserver()
    }


    private fun initRecycler() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
            adapter = postListAdapter
        }
    }


    private fun initViewModel() {
        with(applicationContext as App){
            viewModel = androidx.lifecycle.ViewModelProviders.of(this@MainActivity , PostsViewModelFactory(this)).get(
                PostsViewModel::class.java)

        }


        viewModel.getPosts()
    }

    private fun initObserver() {
        viewModel.pagedListPost.observe(this, Observer<PagedList<Post>> {
            Log.d(MainActivity::class::java.name, "Posts: ${it?.size}")
            postListAdapter.submitList(it)
        })
    }
}
