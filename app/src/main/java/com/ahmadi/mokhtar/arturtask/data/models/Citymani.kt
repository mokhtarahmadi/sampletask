package com.ahmadi.mokhtar.arturtask.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Citymani (
    val total_pages:Int,
    val  page:Int,
    val  posts:List<Post>? = null
):Parcelable