package com.ahmadi.mokhtar.arturtask.data.repository

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.ahmadi.mokhtar.arturtask.data.database.PostRoomDataSource
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.ahmadi.mokhtar.arturtask.data.network.PostNetworkDataSource
import com.ahmadi.mokhtar.arturtask.utils.DATABASE.PAGE_SIZE
import io.reactivex.Observable
import javax.inject.Inject

class PostsRepository @Inject internal constructor(private val postRoomDataSource: PostRoomDataSource, private val postNetworkDataSource: PostNetworkDataSource) {

    fun fetchOrGetPosts() : Observable<PagedList<Post>> = RxPagedListBuilder(postRoomDataSource.getPosts(), PAGE_SIZE)
        .setBoundaryCallback(PageListPostBoundaryCallback(postRoomDataSource,postNetworkDataSource))
        .buildObservable()
}