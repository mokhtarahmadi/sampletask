package com.ahmadi.mokhtar.arturtask.utils

class Constants {
    companion object{
        const val BASE_URL =  "http://citymani.ezrdv.org/"
        const val ENG_LANG = "en"
        const val AM_LANG = "hy"
    }

}