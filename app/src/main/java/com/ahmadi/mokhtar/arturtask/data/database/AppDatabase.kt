package com.ahmadi.mokhtar.arturtask.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.ahmadi.mokhtar.arturtask.utils.DATABASE.DATABASE_POST_VERSION

@Database(entities = [Post::class], version = DATABASE_POST_VERSION , exportSchema = false)
abstract class AppDatabase :RoomDatabase(){
    abstract fun postDao():PostDao
}