package com.ahmadi.mokhtar.arturtask.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_post.view.*

class PostsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun render(post: Post) = itemView.run {
        Glide.with(itemView.context).load(post.user_pic).into(profile_image)
        Glide.with(itemView.context).load(post.photo).into(photo)
        user_name.setText(post.user_name + "")
        date.setText(post.date +"")

    }

    fun clear() = itemView.run {
        itemView.invalidate()
    }
}