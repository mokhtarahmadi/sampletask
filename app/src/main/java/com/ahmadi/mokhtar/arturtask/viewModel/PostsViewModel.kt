package com.ahmadi.mokhtar.arturtask.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.ahmadi.mokhtar.arturtask.data.repository.PostsRepository
import com.ahmadi.mokhtar.arturtask.di.component.DIComponent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PostsViewModel : ViewModel(), DIComponent.Injectable  {

    @Inject
    lateinit var  postsRepository:PostsRepository
    private val compositeDisposable = CompositeDisposable()
    var pagedListPost = MutableLiveData<PagedList<Post>>()

    override fun inject(diComponent: DIComponent) {
        diComponent.inject(this);
    }


    fun getPosts() {
        compositeDisposable.add(postsRepository.fetchOrGetPosts()
            .subscribe({
                pagedListPost.value = it
            },
                {
                it.printStackTrace() }))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}