package com.ahmadi.mokhtar.arturtask.data.repository

import android.util.Log
import androidx.paging.PagedList
import com.ahmadi.mokhtar.arturtask.data.database.PostRoomDataSource
import com.ahmadi.mokhtar.arturtask.data.models.Post
import com.ahmadi.mokhtar.arturtask.data.network.PostNetworkDataSource
import io.reactivex.schedulers.Schedulers

class PageListPostBoundaryCallback(private val postRoomDataSource: PostRoomDataSource,private val postNetworkDataSource: PostNetworkDataSource): PagedList.BoundaryCallback<Post>() {

    private var isRequestRunning = false
    private var requestedPage = 1

    override fun onZeroItemsLoaded() {
        Log.i(TAG, "onZeroItemsLoaded")
        fetchAndStorePosts()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Post) {
        Log.i(TAG, "onItemAtEndLoaded")
        fetchAndStorePosts()
    }

    private fun fetchAndStorePosts() {
        if (isRequestRunning) return

        isRequestRunning = true
        val disposable = postNetworkDataSource.fetchPosts(requestedPage)
            .map {
                it.posts}
            .doOnSuccess { listPost ->
                if (listPost!!.isNotEmpty()) {
                    postRoomDataSource.storePosts(listPost)
                    Log.i(TAG, "Inserted: ${listPost.size}")
                } else {
                    Log.i(TAG, "No Inserted")
                }
                requestedPage++
            }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .ignoreElement()
            .doFinally { isRequestRunning = false }
            .subscribe({
                Log.i(TAG, "Posts Completed") },
                {
                    it.printStackTrace() })

    }

    companion object {
        private const val TAG: String = "PageListPostBoundary "
    }
}