package com.ahmadi.mokhtar.arturtask.di.component

import androidx.annotation.Keep
import com.ahmadi.mokhtar.arturtask.view.MainActivity
import com.ahmadi.mokhtar.arturtask.di.modules.ApiModule
import com.ahmadi.mokhtar.arturtask.di.modules.AppModule
import com.ahmadi.mokhtar.arturtask.viewModel.PostsViewModel
import dagger.Component
import javax.inject.Singleton


@Keep
@Singleton
@Component(modules = [AppModule::class , ApiModule::class])
interface DIComponent {

    interface Injectable{
        fun inject(diComponent: DIComponent)
    }

    fun inject(mainActivity: MainActivity)
    fun inject(postsViewModel: PostsViewModel)
}