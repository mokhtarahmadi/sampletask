package com.ahmadi.mokhtar.arturtask

import android.app.Application
import com.ahmadi.mokhtar.arturtask.di.component.DIComponent
import com.ahmadi.mokhtar.arturtask.di.component.DaggerDIComponent
import com.ahmadi.mokhtar.arturtask.di.modules.ApiModule
import com.ahmadi.mokhtar.arturtask.di.modules.AppModule

class App :Application() {

    lateinit var di : DIComponent
    override fun onCreate() {
        super.onCreate()

        di = DaggerDIComponent
            .builder()
            .apiModule(ApiModule())
            .appModule(AppModule(this))
            .build()

    }
}